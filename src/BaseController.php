<?php

namespace Tetrapak07\Baseapi;

use Phalcon\Mvc\Controller;
use Phalcon\Http\Response;
use Phalcon\Di;
use Tetrapak07\Baseapi\ApiBase;

abstract class BaseController extends Controller
{
    protected $response;
    protected $requestData;

    protected function initialize()
    {
        $this->response = new Response();
        $this->requestData = $this->request->getJsonRawBody();
    }

    public function setJsonResponse($status = 200, $message = '', $data = [])
    {
       $base = new ApiBase();
       return $base->setJsonResponse($status, $message, $data);

    }

    public static function checkVersion()
    {
        $di = new Di();
        return $di::getDefault()->getConfig()->api->appVersion->toArray();
    }

}
