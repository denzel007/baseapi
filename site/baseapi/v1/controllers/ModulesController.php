<?php

namespace Baseapi\V1\Controllers;

use Tetrapak07\Baseapi\BaseController;

class ModulesController extends BaseController
{
    public function all()
    {
        $modules = [];
        $modules  = $this->modelsManager->createQuery("SELECT * FROM \App\Models\Modules")
                             ->execute()->toArray();
        if (count($modules)>1) {  
          return $this->setJsonResponse($modules, 200, 'Id not set'); 
        }
        
        return $this->setJsonResponse(200, 'Empty');
    }
    
    public function show($id = false)
    {
        if (!$id) {
          return $this->setJsonResponse(400, 'Id not set'); 
        }
        
        return $this->setJsonResponse(200, 'OK', $id);
    }
    
    public function register()
    {
        if (empty($this->requestData->email)) {
            return $this->setJsonResponse(400, EMAIL_EMPTY);
        }
    }    
}
