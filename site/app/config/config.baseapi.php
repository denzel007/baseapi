<?php

defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: realpath(dirname(__FILE__) . '/../..'));
defined('DS') || define('DS', DIRECTORY_SEPARATOR);
defined('APP_PATH') || define('APP_PATH', BASE_PATH . ''.DS.'app');
defined('VENDOR_COMPANY') || define('VENDOR_COMPANY','tetrapak07');
defined('MODULES_PATH') || define('MODULES_PATH', BASE_PATH.DS.'vendor'.DS.'tetrapak07');

return new \Phalcon\Config([
    
    'modules' => [
        
      # ADD THIS PART IN YOUR MAIN CONFIG and remove this file from you root config dir NOT FROM VENDOR DIR (!)  
      'baseapi' => [
          'apiUrlPart' => 'api/v1',
          'apiNamespace' => 'Baseapi\\V1',
          'baseApiDir' => BASE_PATH . ''.DS.'baseapi'.DS.'',
          'baseApiV1Dir' => BASE_PATH . ''.DS.'baseapi'.DS.'v1'.DS,
          'baseApiV1DirC' => BASE_PATH . ''.DS.'baseapi'.DS.'v1'.DS.'controllers'.DS,  
          'controllersNamespace' => 'Baseapi\\V1\\Controllers\\'
        ]  

    ],
    
]);

