<?php

namespace Tetrapak07\Baseapi;

use Phalcon\Mvc\Controller;
use Supermodule\ControllerBase as SupermoduleBase;
use Phalcon\Mvc\Micro as MvcMicro;
use Phalcon\DI\FactoryDefault as FactoryDefault;
use Phalcon\Mvc\Micro\Collection as MicroCollection;
use Tetrapak07\Multilang\MultilangBase;

class ApiBase extends Controller
{     
    protected function initialize()
    {
        
    }

    public function onConstruct()
    {
       if (!SupermoduleBase::checkAndConnectModule('baseapi')) {
            header("Location: /error/show404");
            exit;
       } 
       $this->request = new \Phalcon\Http\Request();
       $this->requestData = $this->request->getJsonRawBody();
       $this->multilang = new MultilangBase();
    }
    
    public function indexAction(...$params)
    {
       $version = '1';
       $requestType = $_SERVER['REQUEST_METHOD'];
       
       $versionExpl = explode('/api/', $_SERVER['REQUEST_URI']);
       
       if (isset($versionExpl[1])) {
          $versionExpl2 = explode('/', $versionExpl[1]);
          
          $version = trim($versionExpl2[0]);
       }
       #echo '$version: '.$version;exit; 
       
       if (count($params) == 0) {
           return $this->getDoc($version);
       }
       
       $requestType = strtolower($requestType);
       #echo ' $requestType: '. $requestType;exit; 
        try {
            
          $controller = isset($params[0]) ? $params[0] : '';  
          $method = isset($params[1]) ? $params[1] : false;
          $param1 = isset($params[2]) ? $params[2] : false;
          $param2 = isset($params[3]) ? $params[3] : false;
          
          if (!(preg_match('/[0-9]+/i', $method))) {
             $apiUrl = $method ? $controller.'/'.$method : $controller;
          } else {
             $apiUrl = $controller;  
          }

          return $this->apiData($controller, $method, $requestType, $apiUrl, $param1, $param2, $version);
          
          } catch (\Exception $e) {

          return $this->setJsonResponse(500, $e->getMessage()); 
        }

    }
    
    public function getDoc($version)
    {
        $response = new \Phalcon\Http\Response();
        $response->redirect('/apidoc/'.$version.'/index.html');
        #$response->setContent(file_get_contents($this->config->modules->baseapi->$version->baseApiDirV.'index.html'));
        return $response;
    }
    
    protected function apiData($controller, $method, $type, $url, $param1, $param2, $version)
    {

        $cntrl = $this->config->modules->baseapi->$version->controllersNamespace.ucfirst($controller.'Controller');
        #echo '$cntrl: '.$cntrl;exit;
        $custom = false;
        if (in_array($controller, $this->config->modules->baseapi->customResources->toArray())) {
            $custom = true;
        }
         
        if (class_exists($cntrl)) {
        $cntrl = new $cntrl();
        
        } else {
          return $this->setJsonResponse(404, "RESOURCE_NOT_FOUND");   
        } 
        $arrMethods = get_class_methods($cntrl);
        if ($custom) {
            
             $mtd = $type.ucfirst($method);
        }
        #echo '; $mtd: '.$mtd.'; $type: '.$type;exit;
        if (!$custom) {
            if (!$method && ($type == 'get') ) {
                $mtd = 'index';
            } elseif ($method && $type == 'get' /*&& (preg_match('/[0-9]+/i', $method))*/) {
                $param1 = $method; 
                $mtd = 'show';
            }  elseif ($method && $type == 'put' /*&& (preg_match('/[0-9]+/i', $method))*/) {
                $param1 = $method; 
                $mtd = 'update';
            } elseif (!$method && $type == 'post') {
                $mtd = 'create';
            } elseif ($method && $type == 'delete' /*&& (preg_match('/[0-9]+/i', $method))*/) {
                $param1 = $method; 
                $mtd = 'delete';
            } else {
               return $this->setJsonResponse(405, "METHOD_NOT_ALLOWED");  
            }
        }
        
        if (in_array($controller, $this->config->modules->baseapi->customResources->toArray())) {
           
        }
        #echo '$mtd: '.$mtd.'; $type: '.$type;exit;
        if (in_array($mtd, $arrMethods)) {
 
            $di = new FactoryDefault();
            $app = new MvcMicro($di);

            $collection = new MicroCollection();

            $collection->setHandler($cntrl);

            if (!$param1) {
                $url = '/'.$this->config->modules->baseapi->$version->apiUrlPart.'/'.$url;
            }    
                
            if($param1) {
                
                if (preg_match('/[0-9,a-z]+/i', $param1)) {
                    $url = '/'.$this->config->modules->baseapi->$version->apiUrlPart.'/'.$url.'/{id:[0-9,a-z]+}'; 
                }
                /*if (preg_match('/[a-z]{2}+/i', $param1)) {
                    $url = '/'.$this->config->modules->baseapi->$version->apiUrlPart.'/'.$url.'/{locale:[a-z]{2}}'; 
                }*/
            }
            
            if (($param1)AND($param2)) {
                $url = '/'.$this->config->modules->baseapi->$version->apiUrlPart.'/'.$url.'/{id:[0-9,a-z]+}/{locale:[a-z]{2}}'; 
            } 

            #echo ' url:'.$url.'; $mtd: '.$mtd.'; $type: '.$type;exit;
            $collection->$type($url, $mtd);
            $app->mount($collection);
            
            $app->notFound(function () use ($app) {
                $response = new \Phalcon\Http\Response();
                $response->setJsonContent(array('status' => 404, 'message' => 'COLLECTION_NOT_FOUND'));
                $response->setStatusCode(404, 'COLLECTION_NOT_FOUND');
                return $response;
            });
            
            return $app->handle();

        } else {
            return $this->setJsonResponse(405, "METHOD_NOT_ALLOWED"); 
        }
 
    }
    
    public function setJsonResponse($status = 200, $message = '', $data = [], $customFields = [])
    {
        $response = new \Phalcon\Http\Response();
        $response->setStatusCode($status, $message);
        $response->setContentType('application/json', 'UTF-8');
        
        $time = time();
        
        $retArr = [
            'status' => $status,
            'timestamp' => $time,
            'message' => $message
        ];
          
        if (isset($data['count'])) {
            $c = (int) $data['count'];
            unset($data['count']);
            $retArr['count'] = $c;   
        }
        
        if (count ($customFields)>0) {
            foreach ($customFields as $keyF => $valF) {
                if (!is_string($valF) OR !$valF OR $valF == 'count') {
                    continue;
                }
                if (isset($data[$valF])) {
                    $cc = $data[$valF];
                    unset($data[$valF]);
                    $retArr[$valF] = $cc;   
                }
            }
        }
      
        if (!empty($data)) {
          $retArr['data'] = $data; 
        }
        
        $response->setJsonContent($retArr);
        
        return $response;
    }
      
    public function setLanguageData($entity, $entityID, $data, $lang = false) 
    {
      return  $this->multilang->setLanguageData($entity, $entityID, $data, $lang);
    }

    public function getLanguageData($entity, $entityID, $lang = false) 
    {  
      return  $this->multilang->getLanguageData($entity, $entityID, $lang);  
    }

    public function getLanguageDataKey($translateKey, $type = '', $id = 0, $lang = false) 
    {
       return  $this->multilang->getLanguageDataKey($translateKey, $type, $id, $lang);
    }
    
}